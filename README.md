Required tools:

- cargo-ndk crate
- Android Studio
- NDK

To install the rust tools run:
`cargo install cargo-ndk`
